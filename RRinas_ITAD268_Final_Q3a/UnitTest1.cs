﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;

namespace RRinas_ITAD268_Final_Q3a
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            Process pa = new Process();
            pa.StartInfo.FileName = @"C:\Test\RRinas_ITAD268_Final_Q3.exe";
            pa.StartInfo.UseShellExecute = false;
            pa.StartInfo.RedirectStandardOutput = true;
            pa.Start();
            pa.WaitForExit();

            string result = pa.StandardOutput.ReadToEnd();

            Assert.AreEqual(result, "1,060.66\r\n");

        }
    }
}
